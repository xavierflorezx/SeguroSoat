# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on('turbolinks:load', ->
	jQuery ->
		subgrades = $('#car_subgrade_id').html()
		console.log(subgrades);
		$('#car_grade_id').change ->
			grade = $('#car_grade_id :selected').text() 
			escaped_grade = grade.replace(/([ #;&,.+*~\':*!^$[\]()=>|\/@])/g, '\\$1')
			options = $(subgrades).filter("optgroup[label='#{escaped_grade}']").html()
			console.log(options)
			if options
				$('#car_subgrade_id').html(options)
			else
				$('#car_subgrade_id').empty()
	
)
class HedgesController < ApplicationController
  before_action :set_hedge, only: [:show, :edit, :update, :destroy]

  # GET /hedges
  # GET /hedges.json
  def index
    @hedges = Hedge.all
  end

  # GET /hedges/1
  # GET /hedges/1.json
  def show
  end

  # GET /hedges/new
  def new
    @hedge = Hedge.new
  end

  # GET /hedges/1/edit
  def edit
  end

  # POST /hedges
  # POST /hedges.json
  def create
    @hedge = Hedge.new(hedge_params)

    respond_to do |format|
      if @hedge.save
        format.html { redirect_to @hedge, notice: 'La Cobertura se creó exitosamente.' }
        format.json { render :show, status: :created, location: @hedge }
      else
        format.html { render :new }
        format.json { render json: @hedge.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hedges/1
  # PATCH/PUT /hedges/1.json
  def update
    respond_to do |format|
      if @hedge.update(hedge_params)
        format.html { redirect_to @hedge, notice: 'La Cobertura se actualizó exitosamente.' }
        format.json { render :show, status: :ok, location: @hedge }
      else
        format.html { render :edit }
        format.json { render json: @hedge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hedges/1
  # DELETE /hedges/1.json
  def destroy
    @hedge.destroy
    respond_to do |format|
      format.html { redirect_to hedges_url, notice: 'La Cobertura se eliminó exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hedge
      @hedge = Hedge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hedge_params
      params.require(:hedge).permit(:name, :maximum_amount)
    end
end

class PaymentPoliciesController < ApplicationController
  before_action :set_payment_policy, only: [:show, :edit, :update, :destroy]

  # GET /payment_policies
  # GET /payment_policies.json
  def index
    @payment_policies = PaymentPolicy.all
  end

  # GET /payment_policies/1
  # GET /payment_policies/1.json
  def show
  end

  # GET /payment_policies/new
  def new
    @payment_policy = PaymentPolicy.new
    @policy= Policy.search_resumen_venta(params[:license_plate])

    unless params[:license_plate].nil?

      unless @policy.nil?
        respond_to do |format|
          format.js  { render 'payment_policies/buscar_placa.js.erb' }
        end
      else
        respond_to do |format|
          format.js  { render 'payment_policies/limpiar_buscar_placa.js.erb' }
        end
      end
    end
 

  end

  # GET /payment_policies/1/edit
  def edit
  end

  # POST /payment_policies
  # POST /payment_policies.json
  def create
    @payment_policy = PaymentPolicy.new(payment_policy_params)

    respond_to do |format|
      if @payment_policy.save
        format.html { redirect_to @payment_policy, notice: 'EL pago de Poliza se creó exitosamente.' }
        format.json { render :show, status: :created, location: @payment_policy }
      else
        format.html { render :new }
        format.json { render json: @payment_policy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payment_policies/1
  # PATCH/PUT /payment_policies/1.json
  def update
    respond_to do |format|
      if @payment_policy.update(payment_policy_params)
        format.html { redirect_to @payment_policy, notice: 'EL pago de Poliza se actualizó exitosamente.' }
        format.json { render :show, status: :ok, location: @payment_policy }
      else
        format.html { render :edit }
        format.json { render json: @payment_policy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payment_policies/1
  # DELETE /payment_policies/1.json
  def destroy
    @payment_policy.destroy
    respond_to do |format|
      format.html { redirect_to payment_policies_url, notice: 'EL pago de Poliza se eliminó exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment_policy
      @payment_policy = PaymentPolicy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_policy_params
      params.require(:payment_policy).permit(:card_number, :name_card_holder, :expiration_date_card, :card_security_code, :number_quota,:policy_id)
    end
  end

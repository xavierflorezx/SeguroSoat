class PoliciesController < ApplicationController
  before_action :set_policy, only: [:show, :edit, :update, :destroy]

  # GET /policies
  # GET /policies.json
  def index
    @policies = Policy.all
  end

  # GET /policies/1
  # GET /policies/1.json
  def show
    respond_to do |format|
      format.html
      format.pdf do
         render template:'policies/resumen', pdf: 'Resumen de Compra', orientation:'Landscape' 
      end
    end
  end

def resumen
render pdf: "resumen"  
end

  # GET /policies/new
  def new
      @policy = Policy.new
    @car= Car.searchLicense_plate(params[:license_plate])
    @policy_existe= Policy.search_resumen_venta(params[:license_plate])
    unless  @policy_existe.nil?
          respond_to do |format|
            format.js  { render 'policies/policy_existe.js.erb' }
          end
    else
    
      unless params[:license_plate].nil?
        puts "no existe"
        unless @car.nil?
          respond_to do |format|
            format.js  { render 'policies/buscar_placa.js.erb' }
          end
        else
          respond_to do |format|
            format.js  { render 'policies/limpiar_buscar_placa.js.erb' }
          end
        end
      end
    end
  end

  # GET /policies/1/edit
  def edit
  end

  # POST /policies
  # POST /policies.json
  def create
    @policy = Policy.new(policy_params)
    @policy.user_id = current_user.id
    respond_to do |format|
      if @policy.save
        PolicyMailer.compra_poliza(@policy).deliver_now
        format.html { redirect_to @policy, notice: 'La Compra de Poliza se creó exitosamente.' }
        format.json { render :show, status: :created, location: @policy }
      else
        format.html { render :new }
        format.json { render json: @policy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /policies/1
  # PATCH/PUT /policies/1.json
  def update
    respond_to do |format|
      if @policy.update(policy_params)
        format.html { redirect_to @policy, notice: 'La Compra de Poliza se actualzó exitosamente.' }
        format.json { render :show, status: :ok, location: @policy }
      else
        format.html { render :edit }
        format.json { render json: @policy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /policies/1
  # DELETE /policies/1.json
  def destroy
    @policy.destroy
    respond_to do |format|
      format.html { redirect_to policies_url, notice: 'La Compra de Poliza se eliminó exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_policy
      @policy = Policy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def policy_params
      params.require(:policy).permit(:total_amount, :valid_since,:hedge_id, :car_id)
    end
  end

class SubgradesController < ApplicationController
  before_action :set_subgrade, only: [:show, :edit, :update, :destroy]

  # GET /subgrades
  # GET /subgrades.json
  def index
    @subgrades = Subgrade.all
  end

  # GET /subgrades/1
  # GET /subgrades/1.json
  def show
  end

  # GET /subgrades/new
  def new
    @subgrade = Subgrade.new
  end

def listasubtipos
  
return "hola"

end

  # GET /subgrades/1/edit
  def edit
  end

  # POST /subgrades
  # POST /subgrades.json
  def create
    @subgrade = Subgrade.new(subgrade_params)

    respond_to do |format|
      if @subgrade.save
        format.html { redirect_to @subgrade, notice: 'La subclase se creó exitosamente.' }
        format.json { render :show, status: :created, location: @subgrade }
      else
        format.html { render :new }
        format.json { render json: @subgrade.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subgrades/1
  # PATCH/PUT /subgrades/1.json
  def update
    respond_to do |format|
      if @subgrade.update(subgrade_params)
        format.html { redirect_to @subgrade, notice: 'La subclase se actualizó exitosamente.' }
        format.json { render :show, status: :ok, location: @subgrade }
      else
        format.html { render :edit }
        format.json { render json: @subgrade.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subgrades/1
  # DELETE /subgrades/1.json
  def destroy
    @subgrade.destroy
    respond_to do |format|
      format.html { redirect_to subgrades_url, notice: 'La subclase se eliminó exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subgrade
      @subgrade = Subgrade.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subgrade_params
      params.require(:subgrade).permit(:name, :bonus, :contribution, :rate, :grade_id)
    end
end

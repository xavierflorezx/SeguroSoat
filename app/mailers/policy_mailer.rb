class PolicyMailer < ApplicationMailer
	def compra_poliza(policy)
		@user = policy.user
		@policy=policy
		mail(to: @user.email, subject: 'Welcome to My Awesome Site')
	end
end

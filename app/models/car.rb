class Car < ApplicationRecord
	belongs_to :subgrade
	has_many :policies

	def self.searchLicense_plate(license_plate)
		car = Car.where('"license_plate" = ?',"#{license_plate}").first 
		unless car.nil?
			return car
		else
			return nil
		end

	end

end

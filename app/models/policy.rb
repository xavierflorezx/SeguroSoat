class Policy < ApplicationRecord
	belongs_to :hedge
	belongs_to :user
	belongs_to :car
	has_one :payment_policy

	def self.search_resumen_venta(license_plate)
		resumen = Policy.joins(:car).where('"cars"."license_plate"=?',"#{license_plate}").last 
		unless resumen.nil?
			return resumen
		else
			return nil
		end
	end
end

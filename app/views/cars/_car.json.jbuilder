json.extract! car, :id, :age, :num_passengers, :cylinder, :tons, :created_at, :updated_at
json.url car_url(car, format: :json)

json.extract! grade, :id, :name, :bonus, :contribution, :rate, :created_at, :updated_at
json.url grade_url(grade, format: :json)

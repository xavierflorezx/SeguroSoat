json.extract! hedge, :id, :name, :maximum_amount, :created_at, :updated_at
json.url hedge_url(hedge, format: :json)

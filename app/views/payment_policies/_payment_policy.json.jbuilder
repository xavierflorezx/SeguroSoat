json.extract! payment_policy, :id, :card_number, :name_card_holder, :expiration_date_card, :card_security_code, :number_quota, :created_at, :updated_at
json.url payment_policy_url(payment_policy, format: :json)

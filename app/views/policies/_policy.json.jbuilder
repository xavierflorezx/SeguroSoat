json.extract! policy, :id, :total_amount, :valid_since, :created_at, :updated_at
json.url policy_url(policy, format: :json)

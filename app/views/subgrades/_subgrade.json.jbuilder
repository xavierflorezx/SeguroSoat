json.extract! subgrade, :id, :name, :bonus, :contribution, :rate, :created_at, :updated_at
json.url subgrade_url(subgrade, format: :json)

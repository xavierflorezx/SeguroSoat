json.extract! taker, :id, :type_document, :number_document, :name, :surname, :email, :phone, :created_at, :updated_at
json.url taker_url(taker, format: :json)

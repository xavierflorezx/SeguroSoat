Rails.application.routes.draw do
  resources :subgrades
	resources :payment_policies
	resources :policies
	resources :hedges
	devise_for :users
	resources :grades
	resources :cars
	resources :takers
	root     "home#index"
	get 'policies/resumen', to: "policies/#resumen"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

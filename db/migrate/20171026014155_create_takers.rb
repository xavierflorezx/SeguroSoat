class CreateTakers < ActiveRecord::Migration[5.0]
  def change
    create_table :takers do |t|
      t.string :type_document
      t.string :number_document
      t.string :name
      t.string :surname
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end

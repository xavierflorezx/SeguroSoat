class CreateCars < ActiveRecord::Migration[5.0]
	def change
		create_table :cars do |t|
			t.string :license_plate
			t.integer :age
			t.integer :num_passengers
			t.integer :cylinder
			t.integer :tons
			t.references :subg, index: true
			t.references :grade, references: :subgrades
			t.timestamps
		end
	end
end

class CreateHedges < ActiveRecord::Migration[5.0]
  def change
    create_table :hedges do |t|
      t.string :name
      t.float :maximum_amount

      t.timestamps
    end
  end
end

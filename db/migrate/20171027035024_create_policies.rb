class CreatePolicies < ActiveRecord::Migration[5.0]
  def change
    create_table :policies do |t|
      t.float :total_amount
      t.date :valid_since

      t.timestamps
    end
  end
end

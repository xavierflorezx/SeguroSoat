class CreatePaymentPolicies < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_policies do |t|
      t.string :card_number
      t.string :name_card_holder
      t.date :expiration_date_card
      t.string :card_security_code
      t.integer :number_quota

      t.timestamps
    end
  end
end

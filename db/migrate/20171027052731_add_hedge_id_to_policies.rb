class AddHedgeIdToPolicies < ActiveRecord::Migration[5.0]
  def change
    add_reference :policies, :hedge, foreign_key: true
  end
end

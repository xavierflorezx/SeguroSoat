class AddCarIdToPolicies < ActiveRecord::Migration[5.0]
  def change
    add_reference :policies, :car, foreign_key: true
  end
end

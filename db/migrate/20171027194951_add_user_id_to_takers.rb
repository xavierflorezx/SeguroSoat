class AddUserIdToTakers < ActiveRecord::Migration[5.0]
  def change
    add_reference :takers, :user, foreign_key: true
  end
end

class AddPolicyIdToPaymentPolicies < ActiveRecord::Migration[5.0]
  def change
    add_reference :payment_policies, :policy, foreign_key: true
  end
end

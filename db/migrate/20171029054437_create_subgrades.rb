class CreateSubgrades < ActiveRecord::Migration[5.0]
  def change
    create_table :subgrades do |t|
      t.string :name
      t.float :bonus
      t.float :contribution
      t.float :rate
      t.references :grade, index: true
      t.timestamps
  end
  end
end

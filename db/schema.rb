# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171030152929) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cars", force: :cascade do |t|
    t.string   "license_plate"
    t.integer  "age"
    t.integer  "num_passengers"
    t.integer  "cylinder"
    t.integer  "tons"
    t.integer  "subg_id"
    t.integer  "grade_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["grade_id"], name: "index_cars_on_grade_id", using: :btree
    t.index ["subg_id"], name: "index_cars_on_subg_id", using: :btree
  end

  create_table "grades", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hedges", force: :cascade do |t|
    t.string   "name"
    t.float    "maximum_amount"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "payment_policies", force: :cascade do |t|
    t.string   "card_number"
    t.string   "name_card_holder"
    t.date     "expiration_date_card"
    t.string   "card_security_code"
    t.integer  "number_quota"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "policy_id"
    t.index ["policy_id"], name: "index_payment_policies_on_policy_id", using: :btree
  end

  create_table "policies", force: :cascade do |t|
    t.float    "total_amount"
    t.date     "valid_since"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "hedge_id"
    t.integer  "user_id"
    t.integer  "car_id"
    t.index ["car_id"], name: "index_policies_on_car_id", using: :btree
    t.index ["hedge_id"], name: "index_policies_on_hedge_id", using: :btree
    t.index ["user_id"], name: "index_policies_on_user_id", using: :btree
  end

  create_table "seed_migration_data_migrations", force: :cascade do |t|
    t.string   "version"
    t.integer  "runtime"
    t.datetime "migrated_on"
  end

  create_table "subgrades", force: :cascade do |t|
    t.string   "name"
    t.float    "bonus"
    t.float    "contribution"
    t.float    "rate"
    t.integer  "grade_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["grade_id"], name: "index_subgrades_on_grade_id", using: :btree
  end

  create_table "takers", force: :cascade do |t|
    t.string   "type_document"
    t.string   "number_document"
    t.string   "name"
    t.string   "surname"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_takers_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "payment_policies", "policies"
  add_foreign_key "policies", "cars"
  add_foreign_key "policies", "hedges"
  add_foreign_key "policies", "users"
  add_foreign_key "takers", "users"
end

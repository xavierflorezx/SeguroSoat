require 'test_helper'

class HedgesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hedge = hedges(:one)
  end

  test "should get index" do
    get hedges_url
    assert_response :success
  end

  test "should get new" do
    get new_hedge_url
    assert_response :success
  end

  test "should create hedge" do
    assert_difference('Hedge.count') do
      post hedges_url, params: { hedge: { maximum_amount: @hedge.maximum_amount, name: @hedge.name } }
    end

    assert_redirected_to hedge_url(Hedge.last)
  end

  test "should show hedge" do
    get hedge_url(@hedge)
    assert_response :success
  end

  test "should get edit" do
    get edit_hedge_url(@hedge)
    assert_response :success
  end

  test "should update hedge" do
    patch hedge_url(@hedge), params: { hedge: { maximum_amount: @hedge.maximum_amount, name: @hedge.name } }
    assert_redirected_to hedge_url(@hedge)
  end

  test "should destroy hedge" do
    assert_difference('Hedge.count', -1) do
      delete hedge_url(@hedge)
    end

    assert_redirected_to hedges_url
  end
end

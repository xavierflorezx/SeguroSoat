require 'test_helper'

class PaymentPoliciesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @payment_policy = payment_policies(:one)
  end

  test "should get index" do
    get payment_policies_url
    assert_response :success
  end

  test "should get new" do
    get new_payment_policy_url
    assert_response :success
  end

  test "should create payment_policy" do
    assert_difference('PaymentPolicy.count') do
      post payment_policies_url, params: { payment_policy: { card_number: @payment_policy.card_number, card_security_code: @payment_policy.card_security_code, expiration_date_card: @payment_policy.expiration_date_card, name_card_holder: @payment_policy.name_card_holder, number_quota: @payment_policy.number_quota } }
    end

    assert_redirected_to payment_policy_url(PaymentPolicy.last)
  end

  test "should show payment_policy" do
    get payment_policy_url(@payment_policy)
    assert_response :success
  end

  test "should get edit" do
    get edit_payment_policy_url(@payment_policy)
    assert_response :success
  end

  test "should update payment_policy" do
    patch payment_policy_url(@payment_policy), params: { payment_policy: { card_number: @payment_policy.card_number, card_security_code: @payment_policy.card_security_code, expiration_date_card: @payment_policy.expiration_date_card, name_card_holder: @payment_policy.name_card_holder, number_quota: @payment_policy.number_quota } }
    assert_redirected_to payment_policy_url(@payment_policy)
  end

  test "should destroy payment_policy" do
    assert_difference('PaymentPolicy.count', -1) do
      delete payment_policy_url(@payment_policy)
    end

    assert_redirected_to payment_policies_url
  end
end

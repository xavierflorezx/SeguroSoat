require 'test_helper'

class SubgradesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subgrade = subgrades(:one)
  end

  test "should get index" do
    get subgrades_url
    assert_response :success
  end

  test "should get new" do
    get new_subgrade_url
    assert_response :success
  end

  test "should create subgrade" do
    assert_difference('Subgrade.count') do
      post subgrades_url, params: { subgrade: { bonus: @subgrade.bonus, contribution: @subgrade.contribution, name: @subgrade.name, rate: @subgrade.rate } }
    end

    assert_redirected_to subgrade_url(Subgrade.last)
  end

  test "should show subgrade" do
    get subgrade_url(@subgrade)
    assert_response :success
  end

  test "should get edit" do
    get edit_subgrade_url(@subgrade)
    assert_response :success
  end

  test "should update subgrade" do
    patch subgrade_url(@subgrade), params: { subgrade: { bonus: @subgrade.bonus, contribution: @subgrade.contribution, name: @subgrade.name, rate: @subgrade.rate } }
    assert_redirected_to subgrade_url(@subgrade)
  end

  test "should destroy subgrade" do
    assert_difference('Subgrade.count', -1) do
      delete subgrade_url(@subgrade)
    end

    assert_redirected_to subgrades_url
  end
end

require 'test_helper'

class TakersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @taker = takers(:one)
  end

  test "should get index" do
    get takers_url
    assert_response :success
  end

  test "should get new" do
    get new_taker_url
    assert_response :success
  end

  test "should create taker" do
    assert_difference('Taker.count') do
      post takers_url, params: { taker: { email: @taker.email, name: @taker.name, number_document: @taker.number_document, phone: @taker.phone, surname: @taker.surname, type_document: @taker.type_document } }
    end

    assert_redirected_to taker_url(Taker.last)
  end

  test "should show taker" do
    get taker_url(@taker)
    assert_response :success
  end

  test "should get edit" do
    get edit_taker_url(@taker)
    assert_response :success
  end

  test "should update taker" do
    patch taker_url(@taker), params: { taker: { email: @taker.email, name: @taker.name, number_document: @taker.number_document, phone: @taker.phone, surname: @taker.surname, type_document: @taker.type_document } }
    assert_redirected_to taker_url(@taker)
  end

  test "should destroy taker" do
    assert_difference('Taker.count', -1) do
      delete taker_url(@taker)
    end

    assert_redirected_to takers_url
  end
end
